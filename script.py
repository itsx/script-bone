#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
import logging
import sys


logger = logging.getLogger('unnamed script')


def parse_args(argv):
    parser = argparse.ArgumentParser(
        description='Some description')
    parser.add_argument('-c', '--config', metavar='FILE', required=True,
                        help='configuration file')
    parser.add_argument('--logfile', metavar='FILE',
                        help='log to a specified file')
    parser.add_argument('--loglevel', metavar='LEVEL', default='DEBUG',
                        help='log level')
    return parser.parse_args(argv)


def configure_logging(filename=None, level=logging.DEBUG):
    if filename:
        handler = logging.FileHandler(filename, encoding='utf-8')
    else:
        handler = logging.StreamHandler()
    formatter = logging.Formatter(
        fmt='%(asctime)s [%(name)s] %(levelname)s: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(level)


def main():
    # configure_logging(filename=args.logfile, level=args.loglevel)
    configure_logging(filename=None, level=logging.DEBUG)

    start_time = datetime.datetime.utcnow()

    print("Euro Truck Simulator is best!")

    try:
        pass
    except Exception:
        return 1
    else:
        end_time = datetime.datetime.utcnow()
        logger.info('Finished in %s', end_time - start_time)

    return 0


if __name__ == '__main__':
    status = main()
    sys.exit(status)
